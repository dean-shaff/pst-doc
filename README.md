## Documentation for SKA PST projects

### Building

First we have to initialize and pull all the submodules, as the documentation uses README files from PST repositories.

```
git submodule update --init --recursive
```

If we've already initialized our git submodules, we can pull from the tips of all the submodules:

```
git submodule update --recursive --remote
```


With poetry installed:

```
poetry install
```

Now build the documentation

```
poetry run sphinx-build -b html source build
```
