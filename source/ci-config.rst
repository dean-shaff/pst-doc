
DSPSR/``dspsr-test`` Continuous Integration Configuration
=========================================================

Both DSPSR and ``dspsr-test`` use the Gitlab Continuous Integration (CI)
framework to build, test, verify, and deploy code in an automated fashion.
Unless otherwise configured, the Gitlab CI framework will run a project's CI
configuration every time a new commit is pushed to any branch. A project's CI
pipeline is defined with a YAML file, necessarily called ``.gitlab-ci.yml``.
A Gitlab CI pipeline consist of stages and jobs. Stages correspond roughly
to build, test, and deploy steps, but they can be freely defined for a given
project. Each stage contains at least one job. A job defines how a given CI stage
might be carried out for a specified environment. For example, a project's build stage might
involve building with different compilers, or on different platforms. The DSPSR
``.gitlab-ci.yml`` file illustrates these concepts:

.. code-block:: yaml

  image: nexus.engageska-portugal.pt/ska-docker/dspsr-build:latest

  services:
    - docker:dind

  variables:
    IMAGE_NAME: dspsr
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DSPSR_DIR: /home/psr/dspsr


  stages:
    - build
    - test
    - deploy

  build:
    stage: build
    script:
      - base_dir=${PWD}
      - ./bootstrap
      - echo "apsr bpsr cpsr2 caspsr mopsr sigproc dada" > backends.list
      - echo $CUDA_INCLUDE_DIR
      - echo $CUDA_LIB_DIR
      - mkdir -p ${DSPSR_DIR}/install
      - mkdir build && cd build
      - ./../configure --enable-shared --prefix=${DSPSR_DIR}/install --with-cuda-include-dir=$CUDA_INCLUDE_DIR --with-cuda-lib-dir=$CUDA_LIB_DIR
      - make -j$(nproc)
      - make install
      - cp -r ${DSPSR_DIR}/install/ ${base_dir}/install
    artifacts:
      paths:
        - install
        - test
    only:
      variables:
        - $CI_PIPELINE_SOURCE != "pipeline"

  test:
    stage: test
    script:
      - base_dir=${PWD}
      - mkdir -p ${DSPSR_DIR}
      - cp -r ${base_dir}/install/ ${DSPSR_DIR}/install
      - ${DSPSR_DIR}/install/bin/test_main ~[cuda][unit],~[cuda][no_file] -v high
      - curl -X POST -F token=${CI_JOB_TOKEN} -F ref=master https://gitlab.com/api/v4/projects/${DSPSR_TEST_ID}/trigger/pipeline
    dependencies:
      - build
    only:
      variables:
        - $CI_PIPELINE_SOURCE != "pipeline"


  deploy:
    stage: deploy
    image: docker:stable
    script:
      - docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_HOST
      - image=$DOCKER_REGISTRY_HOST/$DOCKER_REGISTRY_FOLDER/$IMAGE_NAME
      - version="2.0.0-$CI_COMMIT_SHORT_SHA"
      - docker build -t $image:$version .
      - docker tag $image:$version $image:latest
      - docker push $image:$version
      - docker push $image:latest
    only:
      variables:
        - $CI_PIPELINE_SOURCE == "pipeline"


Walking through this file:

- ``image`` indicates the docker image to use for the build. All of the jobs in all of the stages will be run in this image unless otherwise specified.
- ``services`` defines a list of docker services to use for this pipeline. This is necessary because this project builds a docker image inside a docker image (hence ``docker:dind``, or "docker in docker").
- ``variables`` defines any pipeline environment variables. These are available to any job. In this case, we define variables that are used by the docker in docker service.
- ``stages`` defines the names of the stages in the pipeline. The Gitlab CI framework will execute the stages in the order specified.
- ``build``, ``test``, and ``deploy`` define three jobs. In this case, we've given them the same name as their corresponding stages. If we were targeting multiple platforms, we might give these jobs more descriptive names. Each job consists of multiple parts:
  - ``stage``: Indicates to which stage the job belongs.
  - ``image``: Job specific docker image.
  - ``script``: Defines the series of commands to execute in order to complete the job. The last line of the ``test`` job is what triggers the ``dspsr-test`` CI pipeline.
  - ``artifacts``: Defines any paths that will be preserved and carried over into subsequent stages. This is the mechanism by which one stage can build an executable and the next can run it. By default, each stage's job starts in a clean project directory.
  - ``only``: This defines special conditions under which a job can be run. For the DSPSR deploy stage, we only want it pushing to the SKA docker repo from the master branch.


For the moment, the DSPSR project does the following:

- Build the latest commit
- Build a docker image of the latest commit, pushing it to the SKA docker repository.

Likewise, the ``dspsr-test`` is configured to do the following:

- Run unit tests against Python code.
- Download most recent built version of DSPSR
- Run C++ unit and component tests using newest DSPSR commit
- Run Python regression tests


Note that if any one of the stages fails, the subsequent stages will not proceed. For instance, if
the latest DSPSR commit fails to build, a new docker image will not be pushed
to the SKA docker repository.

The main DSPSR and ``dspsr-test`` CI pipelines are linked through pipeline
triggers. When the DSPSR repo's build stage passes, it triggers the ``dspsr-test``
pipeline to start. For ease in testing CI configuration changes, the ``dspsr-test``
CI pipeline will also start if a developer pushes a new commit to the repo.


Docker Images
~~~~~~~~~~~~~

In order to streamline the Gitlab CI pipeline, we've created a series of docker
images that define environments in which pulsar processing related software
can be built and run. These images are as follows:

- `psrchive-docker <https://gitlab.com/dean-shaff/psrchive-docker>`_: Has two docker files, one that creates an environment in which PSRCHIVE can be built, and one that builds PSRCHIVE itself. The build environment is called ``psrchive-build`` and has an image file hosted `here <https://cloud.docker.com/u/dshaff/repository/docker/dshaff/psrchive-build>`_. The image that builds PSRCHIVE itself is called ``psrchive``, and has an image hosted `here <https://cloud.docker.com/u/dshaff/repository/docker/dshaff/psrchive>`_.
- `psrdada-docker <https://gitlab.com/dean-shaff/psrdada-docker>`_: Given the small dependency footprint of PSRDADA, this repository contains a single Docker file that builds PSRDADA. The PSRDADA image file is hosted `here <https://cloud.docker.com/u/dshaff/repository/docker/dshaff/psrdada>`_.
- `dspsr-docker <https://gitlab.com/ska-telescope/dspsr-docker>`_: Contains a single docker file that creates an environment in which DSPSR can be built. Relies on the ``psrchive`` and ``psrdada`` docker images. Unlike with the ``psrdada-docker`` and ``psrchive-docker`` repos, this project will automatically deploy a docker image called ``dspsr-build`` to the SKA Docker repo.

Ultimately, the ``dspsr-build`` image is used in the ``dspsr-test`` and
DSPSR repositories to build DSPSR.

Updating Gitlab CI environment variables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``dspsr`` and ``dspsr-test`` repositories define a series of environment variables that are used to configure the CI environment, and to hide any secret data, like API keys. One influential environment variable is ``REF_DSPSR_REV``, which defines the reference DSPSR commit that is used in Python commit level regression tests. We can change this value, and any others by going to Settings->CI/CD->Variables on the respective Gitlab page, and modifying the ``Value`` field associated with a given ``Key``.


.. Extending CI Configurations
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~
..
.. Imagine that we want to add LLVM/clang compatibility for DSPSR. In order to test this
.. in the CI pipeline, we could add a job to the ``build`` stage that attempts to compile
.. DSPSR with clang/clang++. Note that for the moment, this would not work, as all the
.. pulsar processing software that comprise DSPSR's dependencies are built with
.. gcc. Nonetheless, we might add this job as follows:
..
.. .. code-block:: yaml
..
..   build-clang:
..     stage: build
..     script:
..       - apt-get install clang
..       - base_dir=${PWD}
..       - ./bootstrap
..       - mkdir install
..       - echo "apsr bpsr cpsr2 caspsr mopsr sigproc dada" > backends.list && ./configure --enable-shared --prefix=${base_dir}/install
..       - export CC=clang
..       - export CXX=clang++
..       - export CXXFLAGS="-stdlib=libstdc++"
..       - make
..       - make install
..       - curl -X POST -F token=${CI_JOB_TOKEN} -F ref=master https://gitlab.com/api/v4/projects/${DSPSR_TEST_ID}/trigger/pipeline
..     artifacts:
..       paths:
..         - install
