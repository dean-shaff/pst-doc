Testing DSPSR
=============

C++ Unit and Component Tests
----------------------------

DSPSR uses `Catch2 <https://github.com/catchorg/Catch2>`_ for C++ based testing.
These tests are bundled with the source code, and are compiled along with
everything else, provided a C++11 compatible compiler is available. DSPSR
provides a single test executable ``test_main``. Tests use a TOML configuration
file, which is located in the ``test/`` subdirectory of the DSPSR source tree.
Before running ``test_main``, it's important to configure two environment variables:

- ``DSPSR_TEST_DIR`` (``./test``): This points to the location of the ``test/`` subdirectory
of the DSPSR source tree. If this variable is not set, ``test_main`` assumes that
the `test` subdirectory is located in the current directory.

- ``DSPSR_TEST_DATA_DIR`` (``./``): This points to the location of DSPSR test
data. For more information on getting this data, go to `Getting DSPSR Test Data`_.


In order to see how to use Catch2 to test DSPSR code, let's write a custom
``dsp::Transformation``, and associated Catch2 unit and integration tests. Here,
the unit tests will ensure that any simple public member functions return the
correct values. The integration test will load in some data from disk,
apply the transformation, and ensure that it produces the correct values.

All of the code from this example is available on the ``test-doc`` branch
of the DSPSR git repository.

Note on building DSPSR
~~~~~~~~~~~~~~~~~~~~~~

The examples here assume that you're not building DSPSR directly in the source
tree. This might mean you create a ``build`` subdirectory in the source tree,
or that you build completely out-of-tree. For example, to build DSPSR in a
``build`` subdirectory, I might do something like the following:

.. code-block:: bash

    [me@host dspsr]$ ./bootstrap
    [me@host dspsr]$ mkdir build && cd build
    [me@host build]$ ./../configure --prefix=path/to/install --enabled-shared
    [me@host build]$ make -j4096 && make install


Example: ``ScalarAdd``
~~~~~~~~~~~~~~~~~~~~~~

The example transformation we'll build takes some input
``dsp::TimeSeries`` and adds a scalar value to it, saving the result in some
output ``dsp::TimeSeries``. This transformation is somewhat trivial, but it illustrates how to use the DSPSR API.

Starting at the base of the DSPSR source tree, let's create a file ``ScalarAdd.h``
in ``Signal/General/dsp``:


.. code-block:: bash

    [me@host dspsr]$ touch Signal/General/dsp/ScalarAdd.h



In this file, let's add the following contents:


.. code-block:: c++

    #ifndef __ScalarAdd_h
    #define __ScalarAdd_h

    #include "dsp/Transformation.h"
    #include "dsp/TimeSeries.h"

    namespace dsp {

    class ScalarAdd : public Transformation<TimeSeries, TimeSeries> {

    public:

      ScalarAdd ();

      ~ScalarAdd ();

      void prepare ();

      void set_scalar (float _scalar) { scalar = _scalar; }

      float get_scalar () const { return scalar; }

    protected:

      virtual void transformation ();

    private:

      float scalar;

    };
    }
    #endif


We have a ``scalar`` private member and associated getters and setters. This will
be the value we add to the input ``dsp::TimeSeries`` object.

``prepare`` is the member function used to set up the transformation. In our case
there isn't much to do, other than maybe resize the output ``dsp::TimeSeries`` to
match the size of the input ``dsp::TimeSeries``.

``transformation`` is the member function that will actually operate on input
time series objects.

Aside on DSPSR design pattern and invocation tracking
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*If you're not interested in understanding the connection between the public*
``ScalarAdd::operate`` *method and the protected* ``ScalarAdd::transformation``
*method, skip to the next section.*

In the ``ScalarAdd.h`` header file, ``transformation`` is not a public method. We have not explicitly
overridden any method that will allow us to actually apply our transformation
to input time series objects. We can look at ``ScalarAdd``'s base classes to figure out where the
``transformation`` method is actually called. In ``Kernel/Classes/Operation.C``
we see that the ``dsp::Operation``
class (the base class of ``dsp::Transformation``) has implemented the ``operate`` public member function:

.. code-block:: c++

    bool dsp::Operation::operate ()
    {
      if (verbose)
        cerr << "dsp::Operation[" << name << "]::operate" << endl;

      if (record_time)
        optime.start();

      if( !can_operate() )
        return false;

      operation_status = 0;

      //! call the pure virtual method defined by sub-classes
      operation();

      if (record_time)
        optime.stop();

      if( operation_status != 0 )
        return false;

      return true;
    }


Next, looking in ``Kernel/Classes/dsp/Transformation.h`` we see that the
``dsp::Transformation`` template class has implemented the ``operation`` member
function, which in turn calls the ``transformation`` member function:

.. code-block:: c++

    template <class In, class Out>
    void dsp::Transformation<In, Out>::operation () try
    {
      ...

      if (Operation::verbose)
        cerr << name("operation") << " transformation" << std::endl;

      transformation ();

      ...
    }
     catch (Error& error) {
       throw error += name("operation");
     }

When we invoke ``ScalarAdd::operate``, we see that ``ScalarAdd``
does not implement the ``operate`` member function. Looking up the ``ScalarAdd``
base class hierarchy, the next base class that implements this method is
the ``Operation`` base class. As we saw, ``dsp::Operation::operate`` calls
``operation``. ``ScalarAdd`` does not implement an ``operation`` member function, but ``Transformation``
does, so that gets called next. ``Transformation::operation`` calls the
``transformation`` member function, which we have implemented in ``ScalarAdd``.

The purpose of walking through this invocation chain is twofold. First, it demonstrates
how one might go about searching through the internal machinery of the DSPSR API
to discover from where certain behaviours originate. Second, it
highlights one of the central design philosophies of DSPSR: Operating on data
should be highly abstracted. This means that programmers interested in creating
their own custom data transformations should not have to worry about the low level
nitty gritty of things like memory management or transferring data to GPUs.
Moreover, these data transformations should be easily insertable into arbitrary
signal processing pipelines. To this end, DSPSR uses a hierarchy of increasingly specialized classes to
represent how data flows through the architecture. Classes like ``Transformation``
are *meant* to be sub-classed. Ideally, the end programmer should concern
themselves with implementing as few of the base class's member functions as
possible to achieve the desired data operation.

Example: ``ScalarAdd`` implementation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now, let's implement the ``ScalarAdd`` class we define in ``Signal/General/dsp/SignalAdd.h``.
First, create a file ``ScalarAdd.C`` in ``Signal/General``:

.. code-block:: bash

    [me@host dspsr]$ touch Signal/General/ScalarAdd.C


Now fill it up with the following:


.. code-block:: c++

    #include "dsp/ScalarAdd.h"

    dsp::ScalarAdd::ScalarAdd () : Transformation<TimeSeries,TimeSeries> ("ScalarAdd", outofplace)
    {
      scalar = 0.0; // initialize scalar to something
    }

    dsp::ScalarAdd::~ScalarAdd () {}

    void dsp::ScalarAdd::prepare ()
    {
      output->copy_configuration (input);
      output->set_state(input->get_state());
      output->set_npol(input->get_npol());
      output->set_nchan(input->get_nchan());
      output->set_ndim(input->get_ndim());
      output->set_ndat(input->get_ndat());
      output->resize(input->get_ndat());
    }


    void dsp::ScalarAdd::transformation ()
    {
      if (! prepared) {
        prepare ();
      }
      if (input->get_order() == dsp::TimeSeries::OrderFPT) {

        float* out_ptr;

        unsigned ndim = input->get_ndim();

        for (unsigned ichan=0; ichan<input->get_nchan(); ichan++) {
          for (unsigned ipol=0; ipol<input->get_npol(); ipol++) {
            const float* in_ptr = input->get_datptr(ichan, ipol);
            out_ptr = output->get_datptr(ichan, ipol);
            for (unsigned idat=0; idat<input->get_ndat(); idat++) {
              for (unsigned idim=0; idim<ndim; idim++) {
                out_ptr[idat*ndim + idim] = scalar + in_ptr[idat*ndim + idim];
              }
            }
          }
        }

      } else {
        throw Error (
          InvalidState,
          "dsp::ScalarAdd::transformation",
          "OrderTFP is not supported"
        );
      }
    }

Now we have to tell Autotools to compile our new class. We'll add ``ScalarAdd.C``
to the ``libdspdsp.a`` or ``libdspdsp.so`` object file that is created when
DSPSR compiles. In ``General/Signal/Makefile.am`` add the following:

.. code-block:: autotools

    nobase_include_HEADERS = dsp/ACFilterbank.h dsp/Detection.h	       \
      ...
      dsp/InverseFilterbankResponse.h \
      dsp/ScalarAdd.h


    libdspdsp_la_SOURCES = optimize_fft.c cross_detect.c cross_detect.h  \
    	...
    	InverseFilterbankResponse.C \
      ScalarAdd.C

Now, when we rebuild DSPSR, our ``dsp::ScalarAdd`` class will be added to the
intermediate object that is created when we build the ``Signal/General``
subdirectory.

Example: Testing ``ScalarAdd``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We're now ready to start writing tests for ``ScalarAdd``. In ``test/`` add
a file called ``test_ScalarAdd.cpp``:

.. code-block:: bash

    [me@host dspsr]$ touch test/test_ScalarAdd.cpp


Let's make a simple test that determines if we can get and set the `scalar`
member of ``ScalarAdd``. In ``test/test_ScalarAdd.cpp``:


.. code-block:: c++

    #include "catch.hpp"

    #include "dsp/ScalarAdd.h"


    TEST_CASE("Can get and set ScalarAdd::scalar", "[ScalarAdd]")
    {
      dsp::ScalarAdd adder;

      float expected_value = 5.0;

      CHECK(adder.get_scalar() == 0); // from constructor

      adder.set_scalar(expected_value);

      REQUIRE(adder.get_scalar() == expected_value);

    }

Now, to add our new test case to ``test_main``, we put the following in ``test/Makefile.am``:


.. code-block:: autotools

    test_main_SOURCES = test_main.cpp \
                      ...
                      InverseFilterbank/InverseFilterbankTestConfig.cpp \
                      test_ScalarAdd.cpp



After rebuilding DSPSR, we should be able to test by calling the ``test_main``
executable.


.. code-block:: bash

    [me@local build]$ ./test/test_main
    dspsr: blocksize=699048 samples or 255.999 MB
    unloading 0.078 seconds: InverseFilterbank.test_no_dedispersion
    dspsr: adding InverseFilterbankResponse to Dedispersion kernel
    dspsr: dedispersion filter length=196608 (minimum=16384) complex samples
    dspsr: blocksize=699048 samples or 255.999 MB
    unloading 0.073 seconds: InverseFilterbank.test_during_dedispersion
    dspsr: dedispersion filter length=196608 (minimum=16384) complex samples
    dspsr: convolution requires at least 0 samples
    dspsr: blocksize=524288 samples or 256 MB
    unloading 0.073 seconds: InverseFilterbank.test_after_dedispersion
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    EventEmitter::emit: non C++11
    dsp::ASCIIObservation::load: pfb_dc_chan=163 invalid. defaulting to 0
    Zapped:  total=0.78125% skfb=0% tscr=0% fscr=0%
    Zapped:  total=0.78125% skfb=0% tscr=0.78125% fscr=0%
    ===============================================================================
    All tests passed (75 assertions in 28 test cases)

Right away, we notice that the program generates a lot of output.
What if we want to isolate the test we just wrote? In order to filter out test
cases with Catch2, we can pass keywords in square brackets to the ``test_main``
executable:

.. code-block:: bash

    [me@local build]$ ./test/test_main [ScalarAdd]
    Filters: [ScalarAdd]
    ===============================================================================
    All tests passed (2 assertions in 1 test case)


The filter we pass to ``test_main`` is the same as the one we put in square
brackets in the call to the ``TEST_CASE`` macro.

This first test is sort of trivial -- let's see if we can load some data into a
``dsp::TimeSeries`` object and actually operate on it. Adding to ``test/test_ScalarAdd.cpp``:


.. code-block:: c++

    TEST_CASE ("Can operate on data", "[ScalarAdd]")
    {

      dsp::ScalarAdd adder;

      Reference::To<dsp::TimeSeries> in = new dsp::TimeSeries;
      Reference::To<dsp::TimeSeries> out = new dsp::TimeSeries;

      in->set_state(Signal::Nyquist);
      in->set_nchan (16);
      in->set_npol (2);
      in->set_ndat (100);
      in->set_ndim (1);
      in->resize (100);

      float* in_ptr;

      for (unsigned ichan = 0; ichan < in->get_nchan(); ichan++) {
        for (unsigned ipol = 0; ipol < in->get_npol(); ipol++) {
          in_ptr = in->get_datptr(ichan, ipol);
          for (unsigned idat = 0; idat < in->get_ndat(); idat++) {
            in_ptr[idat] = 1.0;
          }
        }
      }

      float scalar = 4.0;

      adder.set_input(in);
      adder.set_output(out);
      adder.set_scalar(scalar);

      adder.prepare();

      adder.operate();

      bool allclose = true;
      unsigned nclose = 0;
      unsigned total_size = out->get_ndat() * out->get_nchan() * out->get_ndim() * out->get_npol();
      float* out_ptr;

      for (unsigned ichan = 0; ichan < out->get_nchan(); ichan++) {
        for (unsigned ipol = 0; ipol < out->get_npol(); ipol++) {
          out_ptr = out->get_datptr(ichan, ipol);
          for (unsigned idat = 0; idat < out->get_ndat(); idat++) {
            if (out_ptr[idat] != (1.0 + scalar)) {
              allclose = false;
            } else {
              nclose++;
            }
          }
        }
      }

      std::cerr << nclose << "/" << total_size
        << "(" << 100 * (float) nclose / total_size << "%)" << std::endl;

      REQUIRE(allclose == true);

    }

All we're doing here is filling up an input time series with ones, and then
adding the number four to it. In the end, we check to make sure that every
element of the output time series is equal to five. Running the isolated
test again:

.. code-block:: bash

    [me@local build]$ ./test/test_main [ScalarAdd]
    filters: [ScalarAdd]
    3200/3200(100%)
    ===============================================================================
    All tests passed (3 assertions in 2 test cases)


What if we wanted to load some data in from disk to test our transformation?
The testing utilities provide a means of loading data in directly from a binary
dump file. Once we've included ``util/util.hpp`` we can call
``test::util::load_binary_data``:

.. code-block:: c++

    #include "util/util.hpp"

    ...

    std::vector<float> data;
    std::string file_name = "file.dat";

    test::util::load_binary_data (file_name, data);

``test::util::load_binary_data`` is a convenient tool, but it cannot handle
any sort of meta data -- it simply reads all the bytes from a file into a
container, applying the appropriate type cast along the way. This means we have
to keep track of the type, origin, and dimensionality of the data ourselves.
We can avoid this issue of meta data management by leveraging the DSPSR API to
load data from a wide range of formats directly into a ``dsp::TimeSeries``
object. This is particularly useful when operating on existing astronomical datasets.

Let's create a Python script that generates a DADA file that contains the
same data that we generated in the previous test case:

.. code-block:: python

    # create_dump_data.py

    import numpy as np


    def dump_data():

        header = {
          "HDR_SIZE": "4096",
          "BW": "40",
          "DEC": "-45:59:09.5",
          "DSB": "0",
          "FREQ": "1405.000000",
          "HDR_VERSION": "1.0",
          "INSTRUMENT": "dspsr",
          "MODE": "PSR",
          "NBIT": "32",
          "NCHAN": "16",
          "NDIM": "1",
          "NPOL": "2",
          "OBS_OFFSET": "0",
          "PRIMARY": "dspsr",
          "RA": "16:44:49.28",
          "SOURCE": "J1644-4559",
          "TELESCOPE": "PKS",
          "TSAMP": "0.025",
          "UTC_START": "2019-08-29-04:42:20"
        }

        header_str = "\n".join(["{} {}".format(key, val)
                                for key, val in header.items()])
        header_bytes = str.encode(header_str)
        remaining_bytes = 4096 - len(header_bytes)
        header_bytes += str.encode(
            "".join(["\0" for i in range(remaining_bytes)]))

        arr = np.ones((16*2*100, ), dtype=np.float32) # this is numpy's ``float`` type.

        with open("test_data.dump", "wb") as fd:
            fd.write(header_bytes)
            fd.write(arr.tobytes())


    if __name__ == "__main__":
        dump_data()

This script creates a file called ``test_data.dump`` in the directory in which
the script is run. Note that much of the meta data we include in header of the
generated DADA file is not important for testing, rather they are necessary for
the DADA specification. Once we've moved ``test_data.dump`` to the directory
the DSPSR_TEST_DATA_DIR environment variable points to, we can add the
following test case to ``test_ScalarAdd.cpp``:


.. code-block:: c++


    #include "util/util.hpp

    ...

    TEST_CASE ("Can operate on data from file", "[ScalarAdd]")
    {

      const std::string file_path = test::util::get_test_data_dir() + "/" + "test_data.dump";

      dsp::ScalarAdd adder;

      Reference::To<dsp::TimeSeries> in = new dsp::TimeSeries;
      Reference::To<dsp::TimeSeries> out = new dsp::TimeSeries;

      dsp::IOManager manager;

      manager.set_output(in);

      manager.open(file_path);
      manager.set_block_size(1024);

      while (! manager.get_input()->eod()) {
        manager.operate();
      }


      float scalar = 4.0;

      adder.set_input(in);
      adder.set_output(out);
      adder.set_scalar(scalar);

      adder.prepare();

      adder.operate();

      bool allclose = true;
      unsigned nclose = 0;
      unsigned total_size = out->get_ndat() * out->get_nchan() * out->get_ndim() * out->get_npol();
      float* out_ptr;

      for (unsigned ichan = 0; ichan < out->get_nchan(); ichan++) {
        for (unsigned ipol = 0; ipol < out->get_npol(); ipol++) {
          out_ptr = out->get_datptr(ichan, ipol);
          for (unsigned idat = 0; idat < out->get_ndat(); idat++) {
            if (out_ptr[idat] != (1.0 + scalar)) {
              allclose = false;
            } else {
              nclose++;
            }
          }
        }
      }

      std::cerr << nclose << "/" << total_size
        << "(" << 100 * (float) nclose / total_size << "%)" << std::endl;

      REQUIRE(allclose == true);

    }


Notice that this test case is identical to the first, except for the fact that
we're getting our data in a different manner. Moreover, the ``dsp::IOManager``
object takes care of setting the dimensionality of the input ``dsp::TimeSeries``.

The only issue with this approach is the fact that the name of the test file
is hard coded in the test case. It would be nice if this were configurable.
To this end, DSPSR tests use a TOML configuration file, bundled in the ``test``
subdirectory of the DSPSR source code. Let's add the following lines to
``test/test_config.toml``:


.. code-block:: toml

    [ScalarAdd]
    file_name = "test_data.dump"


In ``test_ScalarAdd.cpp``, we can now add the following lines to the top
of the test case that we just wrote:


.. code-block:: c++

    #include "util/TestConfig.hpp"

    ...

    TEST_CASE ("Can operate on data from file", "[ScalarAdd]")
    {
      test::util::TestConfig test_config;

      std::string file_name = test_config.get_field<std::string>("ScalarAdd.file_name");

      const std::string file_path = test::util::get_test_data_dir() + "/" + file_name;

      ...
    }


Running our test case again:

.. code-block:: bash

    [me@local build]$ ./test/test_main [ScalarAdd]
    Filters: [ScalarAdd]
    3200/3200(100%)
    3200/3200(100%)
    ===============================================================================
    All tests passed (4 assertions in 3 test cases)


In this section we saw how to create a custom subclass of ``dsp::Transformation``
and associated Catch2 based tests. These tests are easily configurable via
a TOML file.


Python Regression Tests
-----------------------

DSPSR is accompanied by a set of Python regression tests, called ``dspsr-test``.
These tests compare two DSPSR builds for an expanding range of test cases.
In a CI environment, ``dspsr-test`` compares the most recent DSPSR master commit to a reference build.
In a development environment, programmers can specify which DSPSR executables
to compare. Moreover, they can add their own test cases simply by adding to
a TOML configuration file. ``dspsr-test`` uses `poetry <https://github.com/sdispater/poetry>`_
for dependency management.

Quick-start
~~~~~~~~~~~

Once Poetry is installed, ensure that the ``python`` (*not* ``python3``)
executable on the ``PATH`` is version Python 3.6 or greater. ``dspsr-test`` is not
designed or intended to be compatible with Python 2. Create a virtual environment
and install all the dependencies:

.. code-block:: bash

    [me@host dspsr-test]$ poetry update


Download DSPSR test data:

.. code-block:: bash

    [me@host dspsr-test]$ mkdir -p test_data
    [me@host dspsr-test]$ poetry run python get_gDMCP_data.py -d test_data


Open up ``verify.config.toml`` in your favorite editor, and modify the
``verify_dspsr_builds.executables`` field to point to two dspsr builds you'd like
to test. Moreover, modify the ``verify_dspsr_builds.data_dir`` field to point
to the path of the directory where DSPSR test data is located.

.. code-block:: toml

    [verify_dspsr_builds]

      executables = [
        "/fred/oz002/users/dshaff/software/install/dspsr_master/bin/dspsr", # This is the reference DSPSR executable
        "/fred/oz002/users/dshaff/software/install/dspsr_SK-dev/bin/dspsr", # This is the build we'd like to verify
      ]

      data_dir = "/fred/oz002/users/dshaff/test_data/" # This is where DSPSR test data is located

Now, run the tests:

.. code-block:: bash

    [me@host dspsr-test]$ poetry run python -m verify.verify_dspsr_builds


Configuring ``dspsr-test``
~~~~~~~~~~~~~~~~~~~~~~~~~~

We can add our own test cases to the verification suite by appending to the
``verify.config.toml`` file in the root of the ``dspsr-test`` directory.
Say, for example, that I'd like to add a test to ensure that both builds
produce the same result when using a high dispersion measure. I could
add something like the following:

.. code-block:: toml


    [[verify_dspsr_builds.params]]
    name ="AfterFilterbank_cuda"
    options ="-E pulsar.par -P t2pred.dat -F 128 -T 1 -cuda 0"
    files = "1644-4559.dada"
    gpu = true

    # add the following lines:
    [[verify_dspsr_builds.params]]
    name ="AfterFilterbank_highDM"
    options ="-E pulsar.par -P t2pred.dat -F 128 -T 1 -D 500"
    files = "1644-4559.dada"

    [run_dspsr]
    ...

Now, when we rerun the verification suite, the "AfterFilterbank_highDM" test
case will be added. Given that the test suite can often take a while to execute,
its convenient to specify the name of the test cases we'd like to run.
We can use the ``-n`` command line parameter to achieve this behavior:

.. code-block:: bash

    [me@host dspsr-test]$ poetry run python -m verify.verify_dspsr_builds -n AfterFilterbank_highDM



Getting DSPSR Test Data
~~~~~~~~~~~~~~~~~~~~~~~

DSPSR test data are hosted courtesy of `gDMCP <https://data-portal.hpc.swin.edu.au/>`_.
gDMCP provides a CKAN derived API to access data. The ``get_gDMCP_data.py`` script will leverage
this API to download all the test data present in the DSPSR Test Datasets area.

Once all of ``dspsr-test`` dependencies have been installed, download the data:

.. code-block:: bash

    [me@host dspsr-test]$ mkdir -p test_data
    [me@host dspsr-test]$ poetry run python get_gDMCP_data.py -d test_data


Generating DSPSR Test Data
~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible to regenerate DSPSR test data if you don't have access to the gDMCP test dataset.


FAQ
---

- How do I increase test verbosity?

Running the ``test_main`` executable with ``-v normal`` will increase verbosity
for testing utility code. Running with ``-v high`` is the same as running the
main ``dspsr`` exectable with ``-V`` option

- How do I filter out test cases?

Pass the name of the filter in square brackets to the ``test_main`` executable:
``test_main [ScalarAdd]`` will filter for a test case that has ``[ScalarAdd]``
in the second argument passed to the ``TEST_CASE`` Catch2 macro.

- Why isn't the ``EventEmitter`` test case working correctly?

I'm not entirely sure at this point. When run outside of a Catch2 environment,
the ``EventEmitter`` works as expected. With Catch2, things seem to go a little
haywire.

- When running ``test_main``, I get error message like the following:

.. code-block:: bash

    ...
    .././../../sources/dspsr_SK-dev/test/test_ScalarAdd.cpp:85: FAILED:
    due to unexpected exception with message:
      test::util::load_toml: file_path is either nonexistent or locked
    ...


This is probably an indication that the DSPSR_TEST_DIR environment variable
has not been set to point to the ``test`` subdirectory of the DSPSR source
tree.
