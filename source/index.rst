Welcome to PST's Documentation!
===============================

.. toctree::
   :maxdepth: 2

   testing
   ci-config
   rfi-test-cases
   rfi-injection
   psr-gen
   pfb-inversion-verification

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
